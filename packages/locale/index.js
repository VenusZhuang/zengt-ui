﻿import Vue from 'vue';
import _config from '../../src/create/config';
import deepAssign from '../../src/create/deep-assign';
import defaultMessages from './lang/zh-CN';

const proto = Vue.prototype;
const defaultLang = 'zh-CN';
const locale = {
  install() {
    if (proto[_config.langname]) {
      return;
    }
    Vue.util.defineReactive(proto, _config.langname, defaultLang);
    Vue.util.defineReactive(proto, _config.messagename, { [defaultLang]: defaultMessages });
  },

  use(lang, messages) {
    proto[_config.langname] = lang;
    this.add({ [lang]: messages });
  },

  add(messages = {}) {
    deepAssign(proto[_config.messagename], messages);
  }
};

locale.install();

export default locale;
