﻿import Vue from 'vue';
import use from './use';

const isServer = Vue.prototype.$isServer;

function isDef(value) {
  return value !== undefined && value !== null;
}

function isObj(x) {
  const type = typeof x;
  return x !== null && (type === 'object' || type === 'function');
}

function get(object, path) {
  const keys = path.split('.');
  let result = object;

  keys.forEach(key => {
    result = isDef(result[key]) ? result[key] : '';
  });

  return result;
}

const camelizeRE = /-(\w)/g;
function camelize(str) {
  return str.replace(camelizeRE, (_, c) => c.toUpperCase());
}

function isAndroid() {
  /* istanbul ignore next */
  return isServer ? false : /android/.test(navigator.userAgent.toLowerCase());
}

function device() {
  var ua = navigator.userAgent.toLowerCase();
  /**
  * 设备检测函数
  * @param  {String} needle [特定UA标识]
  * @return {Boolean}
  */
  function deviceDetect(needle) {
    needle = needle.toLowerCase();
    return ua.indexOf(needle) !== -1;
  }
  /**
   * 获取浏览器版本
   * @param  {String} nece [UA中带有版本信息的部分字符串]
   * @return {Number}      [版本号]
   */
  function getVersion(nece) {
    var arr = nece.split('.');
    return parseFloat(arr[0] + '.' + arr[1]);
  }
  var device = {
    isIOS: deviceDetect('iPhone') || deviceDetect('iPad') || deviceDetect('iPod'),
    isAndroid: deviceDetect('Android'),
    isUCBrowser: deviceDetect('UCBrowser'),
    isQQBrowser: deviceDetect('MQQBrowser'),
    isWeixin: deviceDetect('MicroMessenger')
  };

  device.qqBrowserVersion = device.isQQBrowser ? getVersion(ua.split('mqqbrowser/')[1]) : 0;
  device.ucBrowserVersion = device.isUCBrowser ? getVersion(ua.split('ucbrowser/')[1]) : 0;

  return device;
}

function range(num, min, max) {
  return Math.min(Math.max(num, min), max);
};

export {
  use,
  get,
  range,
  isObj,
  isDef,
  isServer,
  camelize,
  isAndroid,
  device
};
