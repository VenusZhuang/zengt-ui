﻿import useBem from '../../../src/create/bem';
import useSfc from './sfc';
import useI18n from './i18n';

export default function(name) {
  name = 'zt-' + name;
  return [useSfc(name), useBem(name), useI18n(name)];
}
