﻿/**
 * add Vue-Router support
 */

export default {
  props: {
    url: String,
    replace: Boolean,
    to: [String, Object]
  },
  methods: {
    routerLink() {
      const { to, url, replace } = this;
      this.ForPage(to, replace, url);
    },

    ForPage(to, replace, url) {
      const { $router } = this;
      if (to && $router) {
        $router[replace ? 'replace' : 'push'](to);
      } else if (url) {
        replace ? location.replace(url) : location.href = url;
      };
    }
  }
};
