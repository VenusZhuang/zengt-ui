﻿const namespan = 'zt';
const langname = '$' + namespan + 'Lang';
const messagename = '$' + namespan + 'Messages';
module.exports = {
  namespan,
  langname,
  messagename
};
module.exports.default = module.exports;
