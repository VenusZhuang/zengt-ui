﻿/**
 * component mixin
 */
import { get, camelize } from '../../packages/utils';
import Config from './config';

export default {
  computed: {
    $t() {
      const { name } = this.$options;
      const prefix = name ? camelize(name) + '.' : '';

      if (!this[Config.messagename]) {
        if (process.env.NODE_ENV !== 'production') {
          console.error('[__Msg] Locale not correctly registered');
        }
        return () => '';
      };

      const messages = this[Config.messagename][Config.langname];
      return (path, ...args) => {
        const message = get(messages, prefix + path) || get(messages, path);
        return typeof message === 'function' ? message.apply(null, args) : message;
      };
    }
  }
};
