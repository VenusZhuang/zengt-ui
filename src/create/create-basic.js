﻿/**
 * Create a basic component with common options
 */
import '../../packages/locale';
import bem from './bem';
import i18n from './i18n';
import { isDef } from '../../packages/utils';
import _config from './config';

const install = function(Vue) {
  Vue.component(this.name, this);
};

export default {
  Config: _config,
  Create: function(sfc) {
    const namespan = _config.namespan;
    sfc.name = namespan + '-' + sfc.name;
    sfc.install = sfc.install || install;
    sfc.mixins = sfc.mixins || [];
    sfc.mixins.push(i18n, bem);
    sfc.methods = sfc.methods || {};
    sfc.methods.isDef = isDef;
    return sfc;
  }
};
