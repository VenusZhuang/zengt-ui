﻿/**
 * Create a component with common options
 */

import Config from '../../src/create/config';
import createBasic from './create-basic';
import RouterLink from '../../src/mixins/router-link';
import Info from '../../packages/info';
import Icon from '../../packages/icon';
import Loading from '../../packages/loading';
import Cell from '../../packages/cell';
import CellGroup from '../../packages/cell-group';

function isVaild(name) {
  let _val = {};
  if (name !== 'info' || name !== 'icon') {
    _val = Object.assign(_val || {}, {
      Info,
      Icon,
      Loading,
      Cell,
      CellGroup
    });
  };
  return _val;
}

export default {
  Config: Object.assign(createBasic.Config || {}, Config),
  Create: function(sfc) {
    const _imp = isVaild(sfc.name);
    sfc.components = Object.assign(sfc.components || {}, _imp);
    sfc.mixins = sfc.mixins || [];
    sfc.mixins.push(RouterLink);
    return createBasic.Create(sfc);
  }
};
